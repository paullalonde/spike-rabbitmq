require 'bunny'

class RabbitReader

  def initialize
    @connection = Bunny.new

    @connection.start
    @channel = @connection.create_channel
    @queue = @channel.queue("bunny.examples.hello_world", :auto_delete => true)

    @queue.subscribe do |delivery_info, metadata, payload|
      puts "Received #{payload}"
    end
  end

  def cleanup
    @connection.close
  end
end
