#!/usr/bin/env ruby
# encoding: utf-8

require "rubygems"
require "bunny"

STDOUT.sync = true

conn = Bunny.new
conn.start

ch = conn.create_channel
q  = ch.queue("bunny.examples.hello_world", :auto_delete => true)
x  = ch.default_exchange

# q.subscribe do |delivery_info, metadata, payload|
#   puts "Received #{payload}"
# end

i = 1

while i <= 100
  x.publish("Hello! #{i}", :routing_key => q.name)
  i += 1
end

#sleep 1.0
conn.close
