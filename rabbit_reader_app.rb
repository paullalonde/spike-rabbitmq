require './rabbit_reader'

class RabbitReaderApp

  def initialize
    @reader = RabbitReader.new
  end

  def call env
    [200, {"Content-Type" => "text/html"}, ["Hello Rack Participants"]]
  end
end
